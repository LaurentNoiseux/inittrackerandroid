﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{


    //Main Panel
    public GameObject verticaLayoutPanel, prefab;
    public Button buttonShowPanelAddCreature;

    //Panel Add Creature
    public GameObject panelAddCreature;
    public Button buttonConfirmAdd, buttonCancelAdd;
    public InputField inputName, inputInit;

    //Swipe
    private Vector3 startPosition, endPosition, moveVec;

    //Others
    private int counter;
    private bool touchStarted;


    // Start is called before the first frame update
    void Start()
    {
        buttonShowPanelAddCreature.onClick.AddListener(ShowPanelAddCreature);
        buttonCancelAdd.onClick.AddListener(HidePanelAddCreature);
        buttonConfirmAdd.onClick.AddListener(AddCreature);

        panelAddCreature.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.touchCount == 1)
        //{
        //    Touch touch = Input.touches[0];
        //    switch(touch.phase)
        //    {
        //        case TouchPhase.Began:
        //        {
        //            startPosition = touch.position;
        //            endPosition = touch.position;
        //            touchStarted = true;
        //        }
        //        break;
        //        case TouchPhase.Moved:
        //        {
        //            endPosition = touch.position;
        //        }
        //        break;
        //        case TouchPhase.Ended:
        //        {
        //            endPosition = touch.position;
        //            moveVec = startPosition - endPosition;
        //            if (moveVec.magnitude >= 80)   // If the swipe is big enough
        //            {
        //                    //if(moveVec.normalized.x > 0.5) // swipe to the left
        //                    //    counter--;
        //                    //if (moveVec.normalized.x < -0.5) // swipe to the right
        //                    //    counter++;
        //                    GameObject newPrefab = Instantiate(prefab, verticaLayoutPanel.transform);
                            
                           
        //                }
        //           //text1.text = counter.ToString();
        //            touchStarted = false;
        //        }
        //        break;
        //    }
        //    //if(touchStarted)
        //    //{
        //    //    moveVec = startPosition - endPosition;
        //    //    if(moveVec.magnitude >= 80)
        //    //    {
        //    //        counter++;
        //    //    }
        //    //    text1.text = counter.ToString();
        //    //}
        
    }
    public void AddCreature()
    {
        ListManager.main.addCreatureToList(inputName.text, inputInit.text);
        HidePanelAddCreature();
    }
    private void ShowPanelAddCreature()
    {
        panelAddCreature.SetActive(true);
    }
    private void HidePanelAddCreature()
    {
        panelAddCreature.SetActive(false);
    }
    //private void OnGUI()
    //{

    //}
}
