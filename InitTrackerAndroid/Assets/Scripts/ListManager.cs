﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListManager : MonoBehaviour
{
    public static ListManager main;
    private List<string> listCreature;
    private int counter;
    public GameObject creaturePrefab;
    private List<GameObject> listPrefab;
    public GameObject verticalLayoutGroup;
    // Start is called before the first frame update
    void Awake()
    {
        main = this;
        listCreature = new List<string>();
        listPrefab = new List<GameObject>();
        counter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void addCreatureToList(string creatureName, string creatureInit)
    {
        listCreature.Add(creatureName);
        printCreatures(true);
    }

    public void printCreatures(bool newCreature)
    {
        if(newCreature)
        {
            GameObject newGm = Instantiate(creaturePrefab, verticalLayoutGroup.transform);
            newGm.name =counter.ToString();
            listPrefab.Add(newGm);
            counter++;
        }

    }
}
